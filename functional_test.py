from selenium import webdriver
import time
#from hello.models import identity
from django.test import LiveServerTestCase,TestCase
from selenium.webdriver.common.keys import Keys
from django.urls import reverse
class NewVisitorTest(LiveServerTestCase):
    def setUp(self):
        self.browser = webdriver.Chrome()
    
    
    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get('http://localhost:8000')
        time.sleep(3)
        find_name = self.browser.find_element_by_name('name')
        find_status = self.browser.find_element_by_name('status')
        find_submit_button = self.browser.find_element_by_name('submit')
        find_name.send_keys('Budi')
        find_status.send_keys('happy')
        find_submit_button.click()

        name= self.browser.find_element_by_name('name').get_attribute('value')
        status = self.browser.find_element_by_name('status').get_attribute('value')
        self.browser.find_element_by_name('yes').click()

        #prev_color_name= self.browser.find_element_by_class_name("name1").value_of_css_property('color')
        prev_color_stat = self.browser.find_elements_by_class_name("status1").value_of_css_property('color')
        print(prev_color_stat)
        #prev_color_time = self.browser.find_element_by_class_name("time1").value_of_css_property('color')
        #self.assertEqual('rgba(0,0,0)',prev_color_name)
        self.assertEqual('rgba(0,0,0,0)',prev_color_stat)
        #self.assertEqual('rgba(0,0,0)',prev_color_time)
        #time.sleep(3)

    def tearDown(self):
        self.browser.close()
    