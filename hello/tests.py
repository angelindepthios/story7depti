from django.test import TestCase,Client
from django.urls import resolve,reverse
from .views import index, confirmation
from .models import identity
#from .forms import confirmationform

class story7test(TestCase):
    def test_story7_url_index_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)
    
    def test_story7_url_confirmation_doesnot_exist(self):
        response = Client().get(reverse('confirmation'))
        self.assertEqual(response.status_code,200)
    
    def test_story7_using_story7_template_index(self):
        responses = Client().get('/')
        self.assertTemplateUsed(responses,'index.html')
    
    def test_story7_using_story7_template_confirmation(self):
        responses = Client().get(reverse('confirmation'))
        self.assertTemplateUsed(responses,'confirmation.html')
    
    def test_story7_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func,index)
    
    def test_story7_confirmation_function(self):
        found = resolve(reverse('confirmation'))
        self.assertEqual(found.func,confirmation)

    def test_creating_object_model_form(self):
        new_identity = identity.objects.create(status='status', name='name')
        count_fill = identity.objects.all().count()
        self.assertEqual(count_fill,1)
        self.assertEqual(str(new_identity),new_identity.name)
    
    def test_post_method_success(self):
        response_post = self.client.post(reverse('confirmation'),{'name':'test','status':'test_status'})
        self.assertEqual(response_post.status_code,302)
        response_page = self.client.get('/')
        response_page_result = response_page.content.decode('utf-8')
        self.assertIn('test',response_page_result)
        self.assertIn('test_status',response_page_result)
        self.assertTemplateUsed('index.html')
    
    def test_confirmation_page_result(self):
        response = self.client.post('',{'name':'test','status':'test_status'})
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed('confirmation.html')