from django.db import models
class identity(models.Model):
    name = models.CharField(max_length=20)
    status = models.CharField(max_length=100)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return "{}".format(self.name)