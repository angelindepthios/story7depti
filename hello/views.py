from django.shortcuts import render,redirect
from .models import identity
from .forms import confirmationform
def index(request):
    if request.method =="POST":
        form = confirmationform(request.POST)
        if form.is_valid():
            x= form.cleaned_data['name']
            y= form.cleaned_data['status']
            x={'name':x,'status':y}
            return render(request,'confirmation.html',x)
    else:
        forms= confirmationform()
    data = identity.objects.all().order_by('-date')
    return render(request,'index.html',{'data':data})

def confirmation(request):
    data= identity.objects.all().order_by('-date')
    if request.method=="POST":
        forms=confirmationform(request.POST)
        if forms.is_valid():
            identity_user = identity()
            identity_user_name = forms.cleaned_data['name']
            identity_user_status = forms.cleaned_data['status']
            identity.objects.create(name=identity_user_name,status=identity_user_status)
            return redirect('index')
    else:
        forms = confirmationform()
    return render(request,'confirmation.html')
